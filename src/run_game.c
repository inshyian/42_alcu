/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   run_game.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/20 14:55:09 by ishyian           #+#    #+#             */
/*   Updated: 2019/07/20 20:15:54 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/alum1.h"

static void		print_line(int len)
{
	int			padding;

	padding = (global()->longest_line - len) / 2;
	while (padding)
	{
		ft_putchar(' ');
		padding--;
	}
	while (len)
	{
		ft_putchar('|');
		len--;
	}
	ft_putchar('\n');
}

static void		print_lines(void)
{
	t_dllist		*curr;

	curr = global()->lines;
	while (curr)
	{
		print_line(curr->content_size);
		curr = curr->next;
	}
}

int				turn(t_dllist *list, int f)
{
	int		n;

	if (f == 1)
	{
		n = ft_calc_bot();
		f = 2;
		CLEAR;
		ft_printf("\033[0;35mCOM takes %d match\n", n);
	}
	else
	{
		n = 0;
		while (n <= 0 || n > (int)list->content_size || n > 3)
		{
			entern(list->content_size);
			n = cin();
		}
		f = 1;
	}
	ft_printf("\n");
	list->content_size -= n;
	return (f);
}

t_dllist		*lister(t_dllist *list)
{
	t_dllist	*del;

	if (list->content_size == 0 && list->prev)
	{
		del = list;
		global()->count_lines--;
		list = del->prev;
		free(del);
		list->next = NULL;
	}
	else if (list->content_size == 0)
	{
		free(list);
		list = NULL;
	}
	return (list);
}

void			run_game(void)
{
	int			f;
	t_dllist	*list;

	f = 2;
	list = global()->lines;
	while (list->next)
		list = list->next;
	while (list)
	{
		ft_printf("\033[0;34mMAP\n");
		print_lines();
		ft_printf("\n");
		f = turn(list, f);
		list = lister(list);
	}
	global()->lines = NULL;
	print_result(f);
}
