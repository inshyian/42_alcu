/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ai.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ddodukal <ddodukal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/20 17:12:58 by yalytvyn          #+#    #+#             */
/*   Updated: 2019/07/20 18:49:59 by ddodukal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/alum1.h"

t_dllist	*ft_get_last(t_dllist *lst, size_t *line, size_t *count, size_t *f)
{
	t_dllist	*tmp;

	tmp = lst;
	if (tmp != NULL)
		*f = tmp->content_size;
	*line = 1;
	while (tmp->next)
	{
		*line += 1;
		*count += tmp->content_size;
		tmp = tmp->next;
	}
	*count += tmp->content_size;
	return (tmp);
}

void		ft_remove_last(t_dllist *lst)
{
	t_dllist	*tmp;

	tmp = lst;
	while (tmp->next->next)
		tmp = tmp->next;
	free(tmp->next);
	tmp->next = NULL;
}

int32_t		ft_ai_rm_last(int32_t count)
{
	if (count < 4)
		return (count);
	if (count == 5)
		return (1);
	if ((count - 3) == 4)
		return (3);
	else if ((count - 2) == 4)
		return (2);
	else if ((count - 1) % 3 == 0)
		return (2);
	else if ((count - 1) == 6)
		return (1);
	else
		return (3);
}

int32_t		ft_ai_rm_one_line(int32_t count)
{
	if (count == 1)
		return (1);
	if (count == 4)
		return (3);
	if (count < 4 && count > 1)
		return (count - 1);
	else if ((count - 1) == 5)
		return (1);
	else if ((count - 1) == 4)
		return (1);
	else if ((count - 1) % 3 == 0)
		return (2);
	else if ((count) % 3 == 0)
		return (1);
	else
		return (3);
}

int32_t		ft_calc_bot(void)
{
	t_dllist	*lst;
	size_t		line;
	size_t		count;
	int32_t		ret;
	size_t		first;

	line = 1;
	count = 0;
	lst = global()->lines;
	lst = ft_get_last(lst, &line, &count, &first);
	if ((global()->count_lines % 2) != 0 || global()->count_lines == 1)
		ret = (ft_ai_rm_one_line(lst->content_size));
	else if (first == 1 && (global()->count_lines % 2) == 0)
		ret = (ft_ai_rm_last(lst->content_size));
	else
		ret = (ft_ai_rm_one_line(lst->content_size));
	return (ret);
}
