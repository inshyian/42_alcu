/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   addition.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ddodukal <ddodukal@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/20 19:36:02 by ddodukal          #+#    #+#             */
/*   Updated: 2019/07/21 18:27:14 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/alum1.h"

void			entern(int n)
{
	if (n >= 3)
		printf("\033[0;32mEnter number from 1 to 3\n");
	if (n == 2)
		printf("\033[0;32mEnter number from 1 to 2\n");
	if (n == 1)
		printf("\033[0;32mEnter number 1\n");
}

int				cin(void)
{
	int		n;
	char	*line;

	line = NULL;
	n = 0;
	get_next_line(0, &line);
	if (line)
		n = ft_atoi(line);
	else
		n = 0;
	free(line);
	return (n);
}

void			print_result(int a)
{
	if (a == 1)
		ft_printf("\033[1;36mCOM win\n");
	else
		ft_printf("You win\n");
}
